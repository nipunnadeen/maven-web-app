/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.nipun.web;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;


public class StudentDao implements StudentDaoInterface {



    @Override
    public void addData(Student st) {

        ResultSet resultsObj = null;
        Connection connectionObj = null;
        PreparedStatement preStatement = null;
        ConnectionPool jdbcObj = new ConnectionPool();

        try {
            DataSource dataSource = jdbcObj.setUpPool();
            jdbcObj.printDbStatus();

            // Performing Database Operation!
            //Making A New Connection Object For Db Transaction
            connectionObj = dataSource.getConnection();
            jdbcObj.printDbStatus();

            preStatement = connectionObj.prepareStatement("INSERT INTO details.student (id,memberId,name,age) VALUES (NULL ,?, ?,?)");
            preStatement.setString(1, st.getMemberId());
            preStatement.setString(2, st.getName());
            preStatement.setInt(3,st.getAge());
            preStatement.executeUpdate();

            //Releasing Connection Object To Pool
        } catch(Exception sqlException) {
            sqlException.printStackTrace();
        } finally {
            try {
                // Closing ResultSet Object
                if(resultsObj != null) {
                    resultsObj.close();
                }
                // Closing PreparedStatement Object
                if(preStatement != null) {
                    preStatement.close();
                }
                // Closing Connection Object
                if(connectionObj != null) {
                    connectionObj.close();
                }
            } catch(Exception sqlException) {
                sqlException.printStackTrace();
            }
        }
        jdbcObj.printDbStatus();

    }


}