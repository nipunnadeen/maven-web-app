
/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.nipun.web;



import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;



import java.io.IOException;
import java.io.StringReader;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;



@Path("/hello")
public class Normal {

    @GET
    @Path("/get")
    public String getMessage(){
        return "hi";
    }

    private Validation service = new Validation();

    final static Logger logger = Logger.getLogger(Validation.class);


    @POST
    @Path("/validate")
    @Consumes("application/xml")
    @Produces("application/xml")
    public Response getDetails(String data) throws IOException, SAXException {



        if (service.valid(data)) {

              return Response.status(200).entity("correct").build();
        } else {
              return Response.status(400).entity("not correct").build();
        }
    }


    public static int PRETTY_PRINT_INDENT_FACTOR = 4;


    @POST
    @Path("/serialize")
    @Consumes(MediaType.APPLICATION_XML)
    @Produces({MediaType.APPLICATION_JSON})
    public Response getBothResponse(String details)
    {

        if (service.valid(details)) {

            try {
                JSONObject xmlJSONObj = XML.toJSONObject(details);
                String jsonPrettyPrintString = xmlJSONObj.toString(PRETTY_PRINT_INDENT_FACTOR);
                System.out.println(jsonPrettyPrintString);
                return Response.status(200).entity(jsonPrettyPrintString).build();
            } catch (JSONException je) {
                System.out.println(je.toString());

                return Response.status(400).entity("not correct").build();
            }
        } else {
            return Response.status(400).entity("not correct").build();
        }
    }


    @POST
    @Path("/jdbc")
    @Consumes("application/xml")
    @Produces("application/xml")
    public Response pushToDB(String data) {

        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            System.out.println("Message: " + e.getMessage());
            logger.error("sorry something wrong",e);

            return Response.status(400).entity("not correct").build();

        }



        if (service.valid(data)) {

            InputSource src = new InputSource();
            src.setCharacterStream(new StringReader(data));

            Document doc = null;
            try {
                doc = builder.parse(src);
            } catch (SAXException e) {
                System.out.println("Message: " + e.getMessage());
                logger.error("sorry something wrong",e);

            } catch (IOException e) {
                System.out.println("Message: " + e.getMessage());
                logger.error("sorry something wrong",e);
            }

            String memberId = doc.getElementsByTagName("id").item(0).getTextContent();
            String name = doc.getElementsByTagName("name").item(0).getTextContent();
            int age = Integer.parseInt(doc.getElementsByTagName("age").item(0).getTextContent());

            StudentDaoInterface  i = new StudentDao();
            i.addData(new Student(memberId,name,age));

            return Response.status(200).entity("correct").build();


        } else {
            return Response.status(400).entity("not correct").build();
        }

    }

}
