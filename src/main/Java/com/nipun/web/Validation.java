
/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.nipun.web;


import org.xml.sax.SAXException;
import javax.xml.XMLConstants;

import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import org.apache.log4j.Logger;



public class Validation {

    final static Logger logger = Logger.getLogger(Validation.class);

    public boolean valid(String xml) {




            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            try {
              Schema schema = factory.newSchema(new File("C:\\Users\\Asus\\Desktop\\webapp\\src\\main\\Java\\com\\nipun\\web\\personal.xsd"));
              Validator validator = schema.newValidator();
              validator.setErrorHandler(new ErrorHandling());
              validator.validate(new StreamSource(new StringReader(xml)));
              System.out.println("successful");


              return true;

            } catch (SAXException e) {

                logger.error("sorry something wrong",e);
                System.out.println("Message: " + e.getMessage());
                return false;


            } catch (IOException e) {
                logger.error("sorry something wrong",e);
                System.out.println("Message: " + e.getMessage());
                return false;
            }

    }


}
