/*
 * Copyright (c) 2019, Entgra (pvt) Ltd. (http://entgra.io) All Rights Reserved.
 *
 * Entgra (pvt) Ltd. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.nipun.web;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.apache.commons.dbcp.ConnectionFactory;
import org.apache.commons.dbcp.DriverManagerConnectionFactory;
import org.apache.commons.dbcp.PoolableConnectionFactory;
import org.apache.commons.dbcp.PoolingDataSource;
import org.apache.commons.pool.impl.GenericObjectPool;

public class ConnectionPool {

    // JDBC Driver Name & Database URL
    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    static final String JDBC_DB_URL = "jdbc:mysql://localhost/details";

    // JDBC Database Credentials
    static final String JDBC_USER = "root";
    static final String JDBC_PASS = "";

    private static GenericObjectPool gPool = null;

    @SuppressWarnings("unused")
    public DataSource setUpPool() throws Exception {
        Class.forName(JDBC_DRIVER);

        // Creates an Instance of GenericObjectPool That Holds Our Pool of Connections Object!
        gPool = new GenericObjectPool();
        gPool.setMaxActive(5);

        // Creates a ConnectionFactory Object Which Will Be Use by the Pool to Create the Connection Object!
        ConnectionFactory cf = new DriverManagerConnectionFactory(JDBC_DB_URL, JDBC_USER, JDBC_PASS);

        // Creates a PoolableConnectionFactory That Will Wraps the Connection Object Created by the ConnectionFactory to Add Object Pooling Functionality!
        PoolableConnectionFactory pcf = new PoolableConnectionFactory(cf, gPool, null, null, false, true);
        return new PoolingDataSource(gPool);
    }

    public GenericObjectPool getConnectionPool() {
        return gPool;
    }

    // This Method Is Used To Print The Connection Pool Status
    public void printDbStatus() {
        System.out.println("Max.: " + getConnectionPool().getMaxActive() + "; Active: " + getConnectionPool().getNumActive() + "; Idle: " + getConnectionPool().getNumIdle());
    }

//    public static void addData(StudentDao student) {
//        ResultSet resultsObj = null;
//        Connection connectionObj = null;
//        PreparedStatement preStatement = null;
//        ConnectionPool jdbcObj = new ConnectionPool();
//
//        try {
//            DataSource dataSource = jdbcObj.setUpPool();
//            jdbcObj.printDbStatus();
//
//            // Performing Database Operation!
//            //Making A New Connection Object For Db Transaction
//            connectionObj = dataSource.getConnection();
//            jdbcObj.printDbStatus();
//
//            preStatement = connectionObj.prepareStatement("INSERT INTO details.student (id,memberId,name,age) VALUES (NULL ,?, ?,?)");
//            preStatement.setString(1, Student.getMemberId());
//            preStatement.setString(2, Student.getName());
//            preStatement.setInt(3,Student.getAge());
//            preStatement.executeUpdate();
//
//            //Releasing Connection Object To Pool
//        } catch(Exception sqlException) {
//            sqlException.printStackTrace();
//        } finally {
//            try {
//                // Closing ResultSet Object
//                if(resultsObj != null) {
//                    resultsObj.close();
//                }
//                // Closing PreparedStatement Object
//                if(preStatement != null) {
//                    preStatement.close();
//                }
//                // Closing Connection Object
//                if(connectionObj != null) {
//                    connectionObj.close();
//                }
//            } catch(Exception sqlException) {
//                sqlException.printStackTrace();
//            }
//        }
//        jdbcObj.printDbStatus();
//    }
}

